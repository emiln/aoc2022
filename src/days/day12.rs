use std::default;

use fnv::FnvHashSet;

fn parse(input: &Vec<String>) -> Vec<Vec<char>> {
    input.iter().map(|s| s.chars().collect()).collect()
}

type Grid = Vec<Vec<char>>;

fn find_all(grid: &Grid, c: char) -> Vec<(usize, usize)> {
    let mut v = vec![];
    for row in 0..grid.len() {
        for col in 0..grid[0].len() {
            if grid[row][col] == c {
                v.push((row, col));
            }
        }
    }
    v
}

fn char_val(c: char) -> usize {
    match c {
        'E' => 'z' as usize,
        'S' => 'a' as usize,
        o => o as usize,
    }
}

fn neighbors(
    coord: &(usize, usize),
    grid: &Grid,
    seen: &mut FnvHashSet<(usize, usize)>,
) -> Vec<(usize, usize)> {
    let mut vec = vec![];
    let val = grid[coord.0][coord.1];
    for offset in [(0i32, 1i32), (0, -1), (1, 0), (-1, 0)] {
        let new_coord = (coord.0 as i32 + offset.0, coord.1 as i32 + offset.1);
        if new_coord.0 >= 0
            && new_coord.0 < grid.len() as i32
            && new_coord.1 >= 0
            && new_coord.1 < grid[0].len() as i32
        {
            let new_val = grid[new_coord.0 as usize][new_coord.1 as usize];
            if char_val(new_val) <= char_val(val) + 1
                && !seen.contains(&(new_coord.0 as usize, new_coord.1 as usize))
            {
                seen.insert((new_coord.0 as usize, new_coord.1 as usize));
                vec.push((new_coord.0 as usize, new_coord.1 as usize));
            }
        }
    }
    vec
}

fn shortest_path(grid: &Grid, starts: Vec<(usize, usize)>, goal: (usize, usize)) -> Option<usize> {
    let mut jumps = 0;
    let mut candidates = starts.clone();
    let mut seen = FnvHashSet::with_capacity_and_hasher(50, Default::default());
    for s in starts {
        seen.insert(s.clone());
    }

    while !candidates.contains(&goal) {
        if candidates.is_empty() {
            return None;
        }
        candidates = candidates
            .iter()
            .flat_map(|coord| neighbors(coord, &grid, &mut seen))
            .collect();
        jumps += 1;
    }

    Some(jumps)
}

pub fn part1(input: &Vec<String>) -> String {
    let grid = parse(input);
    let start = find_all(&grid, 'S');
    let goal = find_all(&grid, 'E');
    shortest_path(&grid, start, *goal.first().unwrap())
        .unwrap()
        .to_string()
}

pub fn part2(input: &Vec<String>) -> String {
    let grid = parse(input);
    let starts = find_all(&grid, 'a');
    let goal = find_all(&grid, 'E');
    shortest_path(&grid, starts, *goal.first().unwrap())
        .unwrap()
        .to_string()
}
