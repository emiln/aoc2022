use std::{borrow::Cow, fs::File};

use fnv::FnvHashSet;
use gif::{Encoder, Frame};

enum Dir {
    Up,
    Down,
    Left,
    Right,
}

struct Move(Dir, i32);

fn parse(s: &String) -> Move {
    let mut iter = s.split_ascii_whitespace();
    let dir = iter.next().unwrap();
    let moves = iter.next().unwrap().parse::<i32>().unwrap();
    match dir {
        "U" => Move(Dir::Up, moves),
        "D" => Move(Dir::Down, moves),
        "L" => Move(Dir::Left, moves),
        "R" => Move(Dir::Right, moves),
        _ => panic!("Illegal direction"),
    }
}

type Snake = Vec<(i32, i32)>;

fn step_segment(head: (i32, i32), tail: (i32, i32)) -> (i32, i32) {
    // No movement necessary
    if (head.0 - tail.0).abs().max((head.1 - tail.1).abs()) <= 1 {
        return tail;
    }
    // Horizontal movement
    if (head.0 - tail.0).abs() == 2 && (head.1 - tail.1).abs() == 0 {
        return (tail.0 + if head.0 > tail.0 { 1 } else { -1 }, tail.1);
    }
    // Vertical movement
    if (head.0 - tail.0).abs() == 0 && (head.1 - tail.1).abs() == 2 {
        return (tail.0, tail.1 + if head.1 > tail.1 { 1 } else { -1 });
    }

    // Diagonal movement
    (
        tail.0 + if head.0 > tail.0 { 1 } else { -1 },
        tail.1 + if head.1 > tail.1 { 1 } else { -1 },
    )
}

fn step_snake(s: &mut Snake, dir: &Dir) {
    let old_head = s.last().unwrap();
    let new_head = match dir {
        Dir::Up => (old_head.0, old_head.1 + 1),
        Dir::Down => (old_head.0, old_head.1 - 1),
        Dir::Left => (old_head.0 - 1, old_head.1),
        Dir::Right => (old_head.0 + 1, old_head.1),
    };
    let head_i = s.len() - 1;
    s[head_i] = new_head;
    for i in (0..head_i).rev() {
        s[i] = step_segment(s[i + 1], s[i]);
    }
}

pub fn part1(input: &Vec<String>) -> String {
    let moves: Vec<Move> = input.iter().map(|s| parse(s)).collect();
    let mut snake = vec![(0, 0), (0, 0)];
    let mut seen = FnvHashSet::with_capacity_and_hasher(10, Default::default());
    for mov in moves {
        for _ in 0..mov.1 {
            step_snake(&mut snake, &mov.0);
            let f = snake.first().unwrap();
            seen.insert((f.0, f.1));
        }
    }
    seen.len().to_string()
}

pub fn part2(input: &Vec<String>) -> String {
    let moves: Vec<Move> = input.iter().map(|s| parse(s)).collect();
    let mut snake: Snake = (0..10).map(|_| (0, 1)).collect();
    let mut seen = FnvHashSet::with_capacity_and_hasher(10, Default::default());
    for mov in moves {
        for _ in 0..mov.1 {
            step_snake(&mut snake, &mov.0);
            let f = snake.first().unwrap();
            seen.insert((f.0, f.1));
        }
    }
    seen.len().to_string()
}

pub fn part2_gif(input: &Vec<String>) -> String {
    let moves: Vec<Move> = input.iter().map(|s| parse(s)).collect();
    let mut snake: Snake = (0..10).map(|_| (0, 1)).collect();
    let mut seen = FnvHashSet::with_capacity_and_hasher(10, Default::default());
    let mut min_x = -30;
    let mut max_x = i32::MIN;
    let mut min_y = -248;
    let mut max_y = i32::MIN;
    let color_map = &[0xFF, 0xFF, 0xFF, 0, 0, 0, 0xFF, 0, 0];
    let (width, height) = (300, 300);
    let mut image = File::create("target/snake.gif").unwrap();
    let mut encoder = Encoder::new(&mut image, width, height, color_map).unwrap();
    let mut buffer: [u8; 300 * 300] = [0; 300 * 300];
    for mov in moves {
        for _ in 0..mov.1 {
            step_snake(&mut snake, &mov.0);
            min_x = min_x.min(snake.last().unwrap().0);
            max_x = max_x.max(snake.last().unwrap().0);
            min_y = min_y.min(snake.last().unwrap().1);
            max_y = max_y.max(snake.last().unwrap().1);
            let f = snake.first().unwrap();
            seen.insert((f.0, f.1));
            // Update snake.
            for seg in &snake {
                buffer[((seg.1 - min_y) * 300 + (seg.0 - min_x)) as usize] = 1;
            }
            let l = snake.last().unwrap();
            buffer[((l.1 - min_y) * 300 + (l.0 - min_x)) as usize] = 2;
            let mut frame = Frame::default();
            frame.delay = 1;
            frame.width = width;
            frame.height = height;
            frame.buffer = Cow::Borrowed(&buffer);
            encoder.write_frame(&frame).unwrap();
            // Clear snake
            for seg in &snake {
                buffer[((seg.1 - min_y) * 300 + (seg.0 - min_x)) as usize] = 0;
            }
        }
    }
    seen.len().to_string()
}
