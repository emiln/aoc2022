use crate::util::to_sections;
use std::collections::VecDeque;
use std::str::FromStr;

#[derive(Debug)]
enum Operation {
    Add(usize),
    Mult(usize),
    Square,
}

impl FromStr for Operation {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match (&s[23..24], &s[25..26]) {
            ("+", _) => Ok(Self::Add(s[25..].parse().unwrap())),
            ("*", "o") => Ok(Self::Square),
            _ => Ok(Self::Mult(s[25..].parse().unwrap())),
        }
    }
}

#[derive(Debug)]
struct Monkey {
    items: VecDeque<usize>,
    inspections: usize,
    op: Operation,
    divisor: usize,
    true_index: usize,
    false_index: usize,
}

fn parse(section: &Vec<&String>) -> Monkey {
    let items = section[1][18..]
        .split(", ")
        .map(|s| s.parse().unwrap())
        .collect();
    let op = Operation::from_str(section[2]).unwrap();
    let divisor = section[3][21..].parse().unwrap();
    let true_index = section[4][29..].parse().unwrap();
    let false_index = section[5][30..].parse().unwrap();
    Monkey {
        divisor,
        false_index,
        inspections: 0,
        items,
        op,
        true_index,
    }
}

fn run(mut monkeys: Vec<Monkey>, rounds: usize, super_worried: bool) -> usize {
    let l = monkeys.len();
    let div: usize = monkeys.iter().map(|m| m.divisor).product();
    for _ in 0..rounds {
        for m in 0..l {
            while let Some(mut worry) = monkeys[m].items.pop_front() {
                let mut monkey = &mut monkeys[m];
                monkey.inspections += 1;
                worry = match monkey.op {
                    Operation::Add(v) => worry + v,
                    Operation::Mult(v) => worry * v,
                    Operation::Square => worry * worry,
                };
                if !super_worried {
                    worry /= 3;
                } else {
                    worry %= div;
                }
                let next_monkey = if worry % monkey.divisor == 0 {
                    monkey.true_index
                } else {
                    monkey.false_index
                };
                monkeys[next_monkey].items.push_back(worry);
            }
        }
    }
    monkeys.sort_by(|m1, m2| m2.inspections.cmp(&m1.inspections));
    monkeys[0].inspections * monkeys[1].inspections
}

pub fn part1(input: &Vec<String>) -> String {
    let sections = to_sections(input);
    let monkeys: Vec<Monkey> = sections.iter().map(|s| parse(s)).collect();
    run(monkeys, 20, false).to_string()
}
pub fn part2(input: &Vec<String>) -> String {
    let sections = to_sections(input);
    let monkeys: Vec<Monkey> = sections.iter().map(|s| parse(s)).collect();
    run(monkeys, 10000, true).to_string()
}
