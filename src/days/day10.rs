use std::str::FromStr;

struct Cpu {
    cycle: usize,
    display: [bool; 6 * 40],
    reg_x: i32,
    signals: Vec<usize>,
}

impl ToString for Cpu {
    fn to_string(&self) -> String {
        let mut s = String::with_capacity(6 * 41 + 1);
        s.push('\n');
        for row in 0..6 {
            for col in 0..40 {
                let b = self.display[row * 40 + col];
                s.push(if b { '█' } else { ' ' });
            }
            s.push('\n');
        }
        s
    }
}

enum Instruction {
    Noop,
    Addx(i32),
}

impl FromStr for Instruction {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match &s[0..4] {
            "noop" => Ok(Instruction::Noop),
            "addx" => Ok(Instruction::Addx(s[5..].parse().unwrap())),
            op => Err(format!("Invalid op: {}", op)),
        }
    }
}

fn step(cpu: &mut Cpu) -> usize {
    let col = ((cpu.cycle - 1) % 40) as i32;
    if cpu.cycle % 40 == 20 {
        cpu.signals.push(cpu.cycle * cpu.reg_x as usize);
    }
    cpu.display[cpu.cycle - 1] = cpu.reg_x >= col - 1 && cpu.reg_x <= col + 1;
    1
}

fn run(instructions: &Vec<Instruction>) -> Cpu {
    let mut cpu = Cpu {
        cycle: 1,
        display: [false; 6 * 40],
        reg_x: 1,
        signals: vec![],
    };

    for inst in instructions {
        match inst {
            Instruction::Noop => cpu.cycle += step(&mut cpu),
            Instruction::Addx(v) => {
                cpu.cycle += step(&mut cpu);
                cpu.cycle += step(&mut cpu);
                cpu.reg_x += v;
            }
        }
    }

    cpu
}

pub fn part1(input: &Vec<String>) -> String {
    let instructions = input
        .iter()
        .map(|line| Instruction::from_str(line).unwrap())
        .collect();
    let cpu = run(&instructions);

    cpu.signals.iter().sum::<usize>().to_string()
}
pub fn part2(input: &Vec<String>) -> String {
    let instructions = input
        .iter()
        .map(|line| Instruction::from_str(line).unwrap())
        .collect();
    let cpu = run(&instructions);

    cpu.to_string()
}
