use std::time::Instant;

mod days;
mod util;

const DAYS: [(fn(&Vec<String>) -> String, fn(&Vec<String>) -> String); 12] = [
    (days::day01::part1, days::day01::part2),
    (days::day02::part1, days::day02::part2),
    (days::day03::part1, days::day03::part2),
    (days::day04::part1, days::day04::part2),
    (days::day05::part1, days::day05::part2),
    (days::day06::part1, days::day06::part2),
    (days::day07::part1, days::day07::part2),
    (days::day08::part1, days::day08::part2),
    (days::day09::part1, days::day09::part2),
    (days::day10::part1, days::day10::part2),
    (days::day11::part1, days::day11::part2),
    (days::day12::part1, days::day12::part2),
];

fn main() {
    for day in 1..=DAYS.len() {
        let i1 = util::example(day);
        let i2 = util::input(day);
        let (f1, f2) = DAYS[day - 1];
        let now = Instant::now();
        let p1e = f1(&i1);
        let dur_e1 = now.elapsed();
        let now = Instant::now();
        let p2e = f2(&i1);
        let dur_e2 = now.elapsed();
        let now = Instant::now();
        let p1i = f1(&i2);
        let dur_i1 = now.elapsed();
        let now = Instant::now();
        let p2i = f2(&i2);
        let dur_i2 = now.elapsed();
        println!("┎ \x1b[34;1mDay {}\x1b[0m", day);
        println!("┠─┬ \x1b[32mExample\x1b[0m");
        println!(
            "┃ ├── p1: \x1b[34;1m{: >9}\x1b[0m ({} \x1b[37;2mµs\x1b[0m)",
            p1e,
            dur_e1.as_micros()
        );
        println!(
            "┃ └── p2: \x1b[34;1m{: >9}\x1b[0m ({} \x1b[37;2mµs\x1b[0m)",
            p2e,
            dur_e2.as_micros()
        );
        println!("┖─┬ \x1b[32mReal input\x1b[0m");
        println!(
            "  ├── p1: \x1b[34;1m{: >9}\x1b[0m ({} \x1b[37;2mµs\x1b[0m)",
            p1i,
            dur_i1.as_micros()
        );
        println!(
            "  └── p2: \x1b[34;1m{: >9}\x1b[0m ({} \x1b[37;2mµs\x1b[0m)",
            p2i,
            dur_i2.as_micros()
        );
        println!("");
    }
}
