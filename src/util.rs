use std::{
    fs::File,
    io::{BufReader, Read},
};

pub fn example(day: usize) -> Vec<String> {
    let file = File::open(format!("./data/{:0>2}/example.txt", day))
        .expect(&format!("Failed opening data file for day {}", day));
    let mut s = String::new();
    BufReader::new(file).read_to_string(&mut s).unwrap();
    s.lines().map(|l| l.to_owned()).collect()
}

pub fn input(day: usize) -> Vec<String> {
    let file = File::open(format!("./data/{:0>2}/input.txt", day))
        .expect(&format!("Failed opening data file for day {}", day));
    let mut s = String::new();
    BufReader::new(file).read_to_string(&mut s).unwrap();
    s.lines().map(|l| l.to_owned()).collect()
}

pub fn to_sections(lines: &Vec<String>) -> Vec<Vec<&String>> {
    let mut vecs = vec![];
    let mut vec = vec![];
    for line in lines {
        if line.is_empty() {
            vecs.push(vec.clone());
            vec.clear();
        } else {
            vec.push(line);
        }
    }
    vecs.push(vec);
    vecs
}
